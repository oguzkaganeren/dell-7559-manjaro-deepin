# dell-7559-manjaro-deepin
![image](https://github.com/oguzkaganeren/dell-7559-manjaro-deepin/blob/master/DeepinScreenshot_20180629182323.png)
![image](https://github.com/oguzkaganeren/dell-7559-manjaro-deepin/blob/master/DeepinScreenshot_20180629182336.png)
![image](https://github.com/oguzkaganeren/dell-7559-manjaro-deepin/blob/master/DeepinScreenshot_20180629182345.png)
![image](https://github.com/oguzkaganeren/dell-7559-manjaro-deepin/blob/master/DeepinScreenshot_20180629182359.png)
![image](https://github.com/oguzkaganeren/dell-7559-manjaro-deepin/blob/master/DeepinScreenshot_20180629182412.png)
![image](https://github.com/oguzkaganeren/dell-7559-manjaro-deepin/blob/master/DeepinScreenshot_20180629182422.png)
![image](https://github.com/oguzkaganeren/dell-7559-manjaro-deepin/blob/master/DeepinScreenshot_20180629182911.png)
![image](https://github.com/oguzkaganeren/dell-7559-manjaro-deepin/blob/master/DeepinScreenshot_20180629183119.png)
![image](https://github.com/oguzkaganeren/dell-7559-manjaro-deepin/blob/master/DeepinScreenshot_20180629183148.png)
![image](https://github.com/oguzkaganeren/dell-7559-manjaro-deepin/blob/master/DeepinScreenshot_20180629183201.png)
### Important
After the restart, you should add this line at boot with press e.
```systemd.mask=mhwd-live.service acpi_osi=! acpi_osi="Windows 2009" ``` 

![image](https://github.com/oguzkaganeren/dell-7559-manjaro-deepin/blob/master/DeepinScreenshot_20180629183206.png)
![image](https://github.com/oguzkaganeren/dell-7559-manjaro-deepin/blob/master/DeepinScreenshot_20180629185234.png)
![image](https://github.com/oguzkaganeren/dell-7559-manjaro-deepin/blob/master/DeepinScreenshot_20180629185250.png)
![image](https://github.com/oguzkaganeren/dell-7559-manjaro-deepin/blob/master/DeepinScreenshot_20180629185305.png)
For Fastest pacman mirror;
```
sudo pacman-mirrors --fasttrack

```
#### For permission
```
sudo chown $USER /media

```
### For Nvidia
```
sudo pacman -S virtualgl lib32-virtualgl lib32-primus primus

sudo mhwd -f -i pci video-hybrid-intel-nvidia-bumblebee

sudo systemctl enable bumblebeed
sudo reboot
optirun -b none nvidia-settings -c :8
```
Before update;
```
sudo pacman-key --refresh-keys
sudo pacman -Syyu

```

```
sudo pacman -S --noconfirm --needed xf86-video-fbdev aria2 git screenfetch ttf-ubuntu-font-family rxvt-unicode unace unrar zip unzip sharutils uudeview arj cabextract speedtest-cli yaourt

yaourt -S ttf-font-awesome ttf-roboto adobe-source-sans-pro-fonts android-studio woeusb-git visual-studio-code-bin


```
